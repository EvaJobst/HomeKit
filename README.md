# iOS Homekit

This app allows the configuration and usage of a smart bulb (preferably a Philips Hue White). The device can be switched on/off and the brightness can be changed. These functions can be activated/deactivated at a determinated time.
<br/><br/>



03/2017 – 06/2017 | iPhone



Overview |  Adding an Automation | Accessory-Details
:-------------------------:|:-------------------------:|:----:
<img src="images/homekit3.png" alt="Overview" width="200px">  |  <img src="images/homekit2.png" alt="Details" width="200px"> | <img src="images/homekit1.png" alt="Map" width="200px">
